package com.iam.rbac.pdp.controller;

import com.google.protobuf.BoolValue;
import com.iam.rbac.pdp.routeguide.CheckAuthZStatusRequest;
import com.iam.rbac.pdp.routeguide.PDPRouteGuideGrpc;
import io.grpc.stub.StreamObserver;

public class PDPServiceImpl extends PDPRouteGuideGrpc.PDPRouteGuideImplBase {

    @Override
    public void isAllowed(CheckAuthZStatusRequest request, StreamObserver<BoolValue> responseObserver) {

        System.out.println(String.format("Received CheckAuthZStatusRequest: user: %s, action: %s, resource: %s and namespace: %s", request.getUser(), request.getAction(), request.getResource(), request.getNamespace()));

        boolean allowed = isAllowed(request.getUser(), request.getAction(), request.getResource(), request.getNamespace());
        BoolValue response = BoolValue.newBuilder().setValue(allowed).build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    private boolean isAllowed(String user, String action, String resource, String namespace){
        // processing logic goes here
        if("benrks".equals(user) && "read".equals(action) && resource.startsWith("/motor/cars")){
            return true;
        }else{
            return false;
        }
    }
}
