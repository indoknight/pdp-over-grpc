package com.iam.rbac.pdp.controller.util;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class EncDecUtil {

    public static String encrypt(String message) {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        standardPBEStringEncryptor.setPassword(System.getenv().get("PK_PASSWD"));
        return standardPBEStringEncryptor.encrypt(message);
    }

    public static String decrypt(String message) {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        standardPBEStringEncryptor.setPassword(System.getenv().get("PK_PASSWD"));
        return standardPBEStringEncryptor.decrypt(message);
    }
}
