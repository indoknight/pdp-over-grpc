import com.iam.rbac.pdp.controller.PDPServiceImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import static com.iam.rbac.pdp.controller.util.EncDecUtil.decrypt;

public class PDPApplicationServer {
    private static final InputStream CERT_STREAM = ClassLoader.getSystemResourceAsStream("java-cert.pem");
    private static final InputStream PRIVATE_KEY_ENCRYPTED_STREAM = ClassLoader.getSystemResourceAsStream("private-key-encrypted.pem");

    public static void main(String[] args) throws Exception {
        final InputStream PRIVATE_KEY_STREAM = new ByteArrayInputStream(decrypt(getEncryptedPrivateKey()).getBytes(StandardCharsets.UTF_8));
        Server server = ServerBuilder
                .forPort(9092)
                .useTransportSecurity(CERT_STREAM, PRIVATE_KEY_STREAM)
                .addService(new PDPServiceImpl()).build();

        server.start();
        server.awaitTermination();
    }

    private static String getEncryptedPrivateKey() {
        return new BufferedReader(
                new InputStreamReader(PRIVATE_KEY_ENCRYPTED_STREAM, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
    }
}
